CREATE DATABASE organigrama;

USE organigrama;

CREATE TABLE task(
    id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(180),
    description VARCHAR(250),
    dateStart TIMESTAMP NOT NULL,
    dateEnd TIMESTAMP NOT NULL
);
