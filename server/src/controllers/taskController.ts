import { Response, Request } from 'express'
import pool from '../database'

class TaskController {

    public async list(req: Request, res: Response) {
        let task = (await pool).query('SELECT * FROM task');
        res.json(task)
    }
    public async getById(req: Request, res: Response) {
        let game = (await pool).query('SELECT * FROM task WHERE ID = ?',[req.params.id ])
        console.log(game);
        res.json(game);        
    }

    public async create(req: Request, res: Response): Promise<void> {
        (await pool).query('INSERT INTO TASK SET ?', [req.body])
        console.log(req.body);
        res.json({ message: 'Task saved' })
    }
    public async delete(req: Request, res: Response) {
        res.json({ text: 'delete a task ' + req.params.id })
    }
    public async update(req: Request, res: Response) {
        res.json({ text: 'update a task ' + req.params.id })
    }




}
export const taskController = new TaskController();