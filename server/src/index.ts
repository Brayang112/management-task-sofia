import express, { application, urlencoded } from 'express';
import indexRoutes from './routes/indexRoutes';
import exampleRouts from './routes/exampleRouts';
import cors from 'cors';


class Server {
    public app = application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void {
        this.app.set('port', process.env.PORT || 3000)
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(urlencoded({extended:false}));
    }

    routes(): void {
        this.app.use('/', indexRoutes);
        this.app.use('/task/', exampleRouts);
    }

    start() {
        this.app.listen(this.app.get('port'))
        console.log('Server on port ' + this.app.get('port'));

    }

}
const server = new Server();
server.start();