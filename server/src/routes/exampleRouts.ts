import { Router } from 'express';
import {taskController} from '../controllers/taskController'

class ExampleRouts {
    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', taskController.list);
        this.router.get('/:id', taskController.getById);
        this.router.post('/', taskController.create);
        this.router.delete('/:id', taskController.delete);
        this.router.put('/:id', taskController.update);
    }

}
const exampleRouts = new ExampleRouts();
export default exampleRouts.router;